package com.thepattybeastlabs.twing;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class IfDebugger extends JFrame {

	private JPanel contentPane;
	private JTextField cond;
	private JTextField cont;
	private JTextField prog;
	private JTextField res;
	private boolean contin;
	private boolean contTo;
	private JButton btnContinue;
	private JButton btnNext;
	private JButton btnContinueTo;
	private JTextArea resA;
	private String text;
	
	public IfDebugger() {
		setResizable(false);
		setTitle("If Debugger");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 572, 485);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCondition = new JLabel("Condition: ");
		lblCondition.setBounds(10, 11, 91, 14);
		contentPane.add(lblCondition);
		
		cond = new JTextField();
		cond.setEditable(false);
		cond.setBounds(111, 8, 445, 20);
		contentPane.add(cond);
		cond.setColumns(10);
		
		JLabel lblParsed = new JLabel("Count: ");
		lblParsed.setBounds(10, 42, 91, 14);
		contentPane.add(lblParsed);
		
		cont = new JTextField();
		cont.setEditable(false);
		cont.setBounds(111, 39, 445, 20);
		contentPane.add(cont);
		cont.setColumns(10);
		
		prog = new JTextField();
		prog.setEditable(false);
		prog.setBounds(111, 70, 445, 20);
		contentPane.add(prog);
		prog.setColumns(10);
		
		JLabel lblParsed_1 = new JLabel("Parsed: ");
		lblParsed_1.setBounds(10, 73, 91, 14);
		contentPane.add(lblParsed_1);
		
		res = new JTextField();
		res.setEditable(false);
		res.setBounds(111, 101, 445, 20);
		contentPane.add(res);
		res.setColumns(10);
		
		JLabel lblResult = new JLabel("Result: ");
		lblResult.setBounds(10, 104, 46, 14);
		contentPane.add(lblResult);
		
		btnContinue = new JButton("Continue");
		btnContinue.setBounds(467, 422, 89, 23);
		contentPane.add(btnContinue);
		
		btnContinueTo = new JButton("Continue To '<'");
		btnContinueTo.setBounds(322, 422, 119, 23);
		contentPane.add(btnContinueTo);
		
		btnNext = new JButton("Next");
		btnNext.setEnabled(false);
		btnNext.setBounds(10, 422, 89, 23);
		contentPane.add(btnNext);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 129, 546, 282);
		contentPane.add(scrollPane);
		
		resA = new JTextArea();
		resA.setEditable(false);
		resA.setLineWrap(true);
		scrollPane.setViewportView(resA);
		btnContinue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contin = true;
			}
		});
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contin = true;
			}
		});
		btnContinueTo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contin = true;
				contTo = true;
			}
		});
		//setVisible(true);
	}
	
	public void setProg(int val, int max){
		StringBuilder b = new StringBuilder(max);
		for(int i = 0; i < max && i < text.length(); i++){
			if(i==val){
				b.append('|');
				b.append(text.charAt(i));
				if(text.charAt(i)=='<'){
					contTo = false;
					contin = false;
				}
			}else{
				
				b.append(text.charAt(i));
			}
		}
		if(val > max){
			b.append("+"+(val-max));val=max+2;
		}
		if(!contTo){
			prog.setText(b.toString());
			try{
			prog.setCaretPosition(val);
			}catch(Exception e){e.printStackTrace();}
			//cont.setCaretPosition(val);
		}
	}
	
	public void setEnd(boolean end){
		if(end){
			btnNext.setEnabled(true);
			btnContinue.setEnabled(false);
			btnContinueTo.setEnabled(false);
			contTo = false;
			contin = false;
		}else{
			btnContinue.setEnabled(true);
			btnContinueTo.setEnabled(true);
			btnNext.setEnabled(false);
		}
	}
	
	public void setText(String text){
		this.text =text;
	}
	
	public void setCount(String text){
		cont.setText(text);
	}
	
	public void setRes(String text){
		res.setText(text);
		res.setCaretPosition(text.length());
		resA.setText(text);
	}
	
	public void setCond(String text){
		cond.setText(text);
	}
	
	public void waitFor(){
		while(!contin){
		try {
			Thread.sleep(32);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}	
		}
		if(!contTo)contin = false;
	}
}
