package com.thepattybeastlabs.twing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javafx.embed.swing.JFXPanel;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.web.WebErrorEvent;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebView;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.filechooser.FileFilter;

import netscape.javascript.JSObject;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;

import com.thepattybeastlabs.twing.Scene.TextHandler;

public class TwingMain extends JFrame {

	public static boolean doVarSetting = true;
	
	public static Compare[] comps = new Compare[]{new Compare(" is ", " eq ") {public boolean c(String n1, String n2) {
		if(n1==null||n2==null)return false;
		if(isNum(n1))n1 = num(n1)+"";
		if(isNum(n2))n2 = num(n2)+"";
		//System.out.println(n1+"=="+n2);
		return n1.equals(n2);
		}},
		new Compare(">", " gt ") {public boolean c(String n1, String n2) {return num(n1)>num(n2);}},
		new Compare("<", " lt ") {public boolean c(String n1, String n2) {return num(n1)<num(n2);}},
		new Compare(">=", " gte ") {public boolean c(String n1, String n2) {return num(n1)>=num(n2);}},
		new Compare("<=", " lte ") {public boolean c(String n1, String n2) {return num(n1)<=num(n2);}}};
	
	public static INIFile config;
	private static JPanel contentPane;
	private static WebView content;
	public static ArrayList<Scene> scenes = new ArrayList<Scene>();
	private static JTextField sceneName;
	private static ArrayList<Command> cmds = new ArrayList<Command>();
	private static SourceViewer sourceViewer;
	private static VarGui varList;
	private static ArrayList<String> history = new ArrayList<String>();
	private static Scene scene;
	private static Random random;
	public static File game;
	public static String storyName;
	private static IfDebugger ifd;
	public static Map<String, String> vars = new HashMap<>();
	public static Map<String, Scene> widgets = new HashMap<>();
	
	/**
	 * Launch the application.
	 */
	public static void main(final String[] args) {
		
		if(!new File("saves").exists())new File("saves").mkdir();
		
		try {
			config = new INIFile("config.ini");
			config.setSaveOnSet(true);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		cmds.add(new Command() {
			public String run(String[] args, String cmd, TextHandler t) {
				cmd = cmd.toLowerCase().replace("set", "").trim();
				String[] splt = null;
				if(cmd.contains("=")){
					splt = cmd.split("=");
				}else{
					splt = cmd.split(" eq ");
				}
				if(splt.length < 2){
					return "Invalid Split";
				}
				String val = splt[1].trim();
				boolean quotes = false;
				if(val.startsWith("\"")){val = val.substring(1);quotes=true;}
				if(val.endsWith("\"")){val = val.substring(0, val.length()-1);quotes=true;}
				String pVal = val;
				if(!quotes)pVal = parseValue(val);
				setVar(splt[0].trim().substring(1), pVal);
				return "";
				//return "set: " +splt[0].trim().substring(1)  + ", " + pVal + ";";
			}
			public String getName() {
				return "set";
			}
		});
		cmds.add(new Command() {
			public String run(String[]cmdA, String cmd, TextHandler t) {
				double r1 = 0;
				double r2 = 0;
				if(cmdA.length > 1){
					r1 = num(parseValue(cmdA[0]));
					r2 = num(parseValue(cmdA[1]));
				}else{
					r2 = num(parseValue(cmdA[0]));
					System.out.println("Warn: rand length invalid for: " + cmd);
				}
				if(r1 > r2){
					double tmp = r2;
					r2 = r1;
					r1 = tmp;
				}
				return random.nextInt((int)(r2-r1+1))+"";
				
			}
			public String getName() {
				return "random";
			}
		});
		cmds.add(new Command() {
			public String run(String[] args, String cmd, TextHandler t) {
				Scene s = getScene(args[1]);
				if(s==null)return "Scene: " + args[1] + " Not Found";
				return s.getHTML(false);
			}
			public String getName() {
				return "display";
			}
		});
		cmds.add(new Command() {

			public String run(String[] args, String cmd, TextHandler t) {
				for(int i = 0; i < args.length; i++){
					if(args[i].startsWith("\"")&&args[i].endsWith("\"")){args[i]=args[i].substring(1, args[i].length()-1);}
				}
				System.out.println(args[1] + ", " + args[2]);
				return "<input type=\"radio\" name=\""+args[1]+"\" value=\""+args[2]+"\"/>";
			}
			
			public String getName() {
				return "radiobutton";
			}
		});
		cmds.add(new Command() {
			public String run(String[] args, String cmd, TextHandler t) {
				int r = random.nextInt(args.length);
				return args[r].trim();
			}
			public String getName() {
				return "either";
			}
		});
		cmds.add(new Command() {
			public String run(String[] args, String cmd, TextHandler t) {
				
				StringBuilder exec = new StringBuilder();
				while(!t.next("<<endwidget>>", 1)){
					exec.append(t.n());
				}
				t.toffset+= "<<endwidget>>".length();
				widgets.put(args[1].substring(1, args[1].length()-1), new Scene("widget", exec.toString()));
				//System.out.println(args[1].substring(1, args[1].length()-1)+"," +exec.toString());
				return "";
			}
			public String getName() {
				return "widget";
			}
		});
		cmds.add(new Command() {
			public String run(String[] args, String cmd, TextHandler t) {
				cmd = cmd.substring(9, cmd.length()-2);
				String[] splt = cmd.split("\\|");
				StringBuilder exec = new StringBuilder();
				while(!t.next("<<endbutton>>", 1)){
					exec.append(t.n());
				}
				t.toffset+= "<<endbutton>>".length();
				System.out.println("WTF "+exec.toString());
				return "<a href='SCENE:"+(splt.length>1?splt[1]:splt[0])+"%EXEC:"+exec.toString()+"' style='background-color: rgb(100,10,10);text-decoration: none;color:white;border: 1px solid black;'>"+splt[0]+"</a>";
			}
			public String getName() {
				return "button";
			}
		});
		cmds.add(new Command() {
			public String run(String[] args, String cmd, TextHandler t) {
				//System.out.println("Parsing iof");
				if(ifd!=null)ifd.setEnd(false);
				cmd = cmd.toLowerCase().replace("if", "").trim();
				StringBuilder internal = new StringBuilder();
				boolean isTrue = condMet(cmd);
				boolean append = true;
				int ifLvl = 0;
				if(ifd!=null)ifd.setText(t.text.substring(t.offset));
				if(ifd!=null)ifd.setCond(cmd);
				while(true){
					char c = t.n();
					//System.out.println();
					if(c=='\0'){
						return "<p style=\"color: red;\">Error[End Of File]</p>";
					}else if(c=='<' && t.n(0)=='<'){
						t.toffset+=1;
						String lCmd = t.text.substring(t.offset+ t.toffset).trim();
						
						//System.out.println("LCMD: "+lCmd);
						if(lCmd.startsWith("if")){
							ifLvl++;
							if(append)internal.append("<<");
						}else if(lCmd.startsWith("endif")){
							if(ifLvl >0){
								ifLvl--;
								if(append)internal.append("<<");
							}else{
								//System.out.println("Offset==" + t.toffset + ", " + lCmd.length());
								//System.out.println("ELSEIFCMD: "+lCmd.substring(0, lCmd.indexOf(">>")+2));
								//t.length+=lCmd.indexOf(">>")+2;
								t.toffset+=lCmd.indexOf(">>")+2;
								//System.out.println("Offset==" + t.toffset);
								if(!isTrue)internal.delete(0, internal.length());
								break;
							}
						}else if(lCmd.startsWith("elseif")){
							System.out.println(ifLvl);
							if(ifLvl >0){
								if(append)internal.append("<<");
							}else{
								t.toffset+=lCmd.indexOf(">>")+2;
								if(!isTrue){
									internal.delete(0, internal.length());
									isTrue = condMet(lCmd.substring(6, lCmd.indexOf(">>")).trim());
								}else{
									append = false;
								}
							}
						}else if(lCmd.startsWith("else")){
							if(ifLvl >0){
								if(append)internal.append("<<");
							}else{
								t.toffset+=6;
								if(!isTrue){
									isTrue = true;
									internal.delete(0, internal.length());
								}else{
									append = false;
								}
							}
						}else{
							if(append)internal.append("<<");
						}
					}else{
						//System.out.print(c + ","+t.toffset);
						if(append)internal.append(c);
					}
					if(ifd!=null)ifd.setCount("Level: " +ifLvl);
					if(ifd!=null)ifd.setProg(t.toffset, t.text.length() - t.offset);
					if(ifd!=null)ifd.setRes(internal.toString());
					if(ifd!=null)ifd.waitFor();
				}
				if(ifd!=null)ifd.setProg(t.toffset, t.text.length() - t.offset);
				if(ifd!=null)ifd.setRes(internal.toString());
				if(ifd!=null)ifd.setEnd(true);
				if(ifd!=null)ifd.waitFor();
				//System.out.println();
				//System.out.println("IF STATMENT("+cmd+", "+condMet(cmd)+"): "+ internal + "[ENDIF("+cmd+")]");
				//return "IF STATMENT("+cmd+", "+condMet(cmd)+"): "+ internal + "[ENDIF("+cmd+")]";
				Scene html = new Scene(cmd,internal.toString());
				html.parent= (t.s.parent==null?t.s : t.s.parent);
				//JOptionPane.showMessageDialog(ifd, html.replace("<br>", "\n"), "HTML OK", JOptionPane.PLAIN_MESSAGE);
				return html.getHTML(false);
			}
			public String getName() {
				return "if";
			}
		});
		cmds.add(new Command() {
			public String run(String[] args, String cmd, TextHandler t) {
				cmd = cmd.toLowerCase().replace("print $", "").trim();
				if(vars.containsKey(cmd)){
					return vars.get(cmd);
				}
				return "<p style=\"color: red;\">\""+cmd+" Does Not Exist\"</p>";
			}
			public String getName() {
				return "print";
			}
		});
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					config.setGroup("ui");
					UIManager.setLookAndFeel(config.get("lookandfeel", UIManager.getSystemLookAndFeelClassName()));
					TwingMain frame = new TwingMain(args);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public static boolean condMet(String cond){
		for(int i = 0; i < comps.length; i++){
			if(cond.contains(comps[i].main) || cond.contains(comps[i].alt)){
				String spltS = comps[i].main;
				if(cond.contains(comps[i].alt)){
					spltS = comps[i].alt;
				}
				String[] splt = cond.split(spltS);
				boolean b=comps[i].c(parseValue(splt[0]), parseValue(splt[1]));
				System.out.println(spltS + " Comp: " + cond + ": " + splt[0] + ", " + splt[1] + ": " + parseValue(splt[0]));
				return b;
			}
		}
		System.err.println(cond + " Invalid");
		return false;
	}

	/**
	 * Create the frame.
	 */
	public TwingMain(final String[] args) {
		//ifd = new IfDebugger();
		//ifd.setVisible(true);
		config.setGroup("General");
		random = new Random();
		random.nextInt(6);
		setTitle("SJTPE - No File - Twine Viewer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 626, 589);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmOpen = new JMenuItem("Open Story...");
		mntmOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser c = new JFileChooser(config.get("game", ""));
				FileFilter f = new FileFilter() {
					public String getDescription() {
						return "HTML File(html,htm)";
					}
					public boolean accept(File f) {
						return f.isDirectory() || f.getName().toLowerCase().endsWith(".html")||
								f.getName().toLowerCase().endsWith(".htm");
					}
				};
				c.addChoosableFileFilter(f);
				c.setFileFilter(f);
				c.showOpenDialog(TwingMain.this);
				if(c.getSelectedFile()!=null){
					new Thread(new Runnable() {
						public void run() {
							try {
								openStory(c.getSelectedFile(), true);
							} catch (IOException e1) {
								JOptionPane.showMessageDialog(TwingMain.this, "Error Opening Story", "Error", JOptionPane.ERROR_MESSAGE);
								e1.printStackTrace();
							}
						}
					}).start();
				}
			}
		});
		mnFile.add(mntmOpen);
		
		JMenu mnLoadSave = new JMenu("Load Save");
		mnFile.add(mnLoadSave);
		
		JMenuItem loadFile = new JMenuItem("Load File...");
		mnLoadSave.add(loadFile);
		
		JSeparator separator_2 = new JSeparator();
		mnFile.add(separator_2);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mnFile.add(mntmExit);
		
		JMenu mnStory = new JMenu("Story");
		menuBar.add(mnStory);
		
		JMenuItem mntmSave = new JMenuItem("New Save...");
		mntmSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser("saves");
				fc.showSaveDialog(TwingMain.this);
				try {
					String f = fc.getSelectedFile().getAbsolutePath();
					if(!f.endsWith(".tgs"))f+=".tgs";
					saveGame(new File(f));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		loadFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser("saves");
				fc.showOpenDialog(TwingMain.this);
				try {
					String f = fc.getSelectedFile().getAbsolutePath();
					if(!f.endsWith(".tgs"))f+=".tgs";
					loadGame(new File(f));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		mnStory.add(mntmSave);
		
		JMenuItem mntmSaveLast = new JMenuItem("Save Last");
		mntmSaveLast.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					saveGame(new File(config.get(game.getAbsolutePath()+"_lastsave", "defaultSave.tgs")));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		mnStory.add(mntmSaveLast);
		
		JMenuItem mntmLoadLast = new JMenuItem("Load Last");
		mnStory.add(mntmLoadLast);
		mntmLoadLast.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Thread(new Runnable() {
					public void run() {
						try {
							loadGame(new File(config.get(game.getAbsolutePath()+"_lastsave", "defaultSave.tgs")));
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				}).start();
				
			}
		});
		
		JSeparator separator_1 = new JSeparator();
		mnStory.add(separator_1);
		
		JMenuItem mntmForward = new JMenuItem("Forward");
		mntmForward.setEnabled(false);
		mnStory.add(mntmForward);
		
		JMenuItem mntmBack = new JMenuItem("Back");
		mntmBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(history.size() > 1){
					new Thread(new Runnable() {
						public void run() {
							openScene(history.get(history.size()-2), false);
							history.remove(history.size()-2);
						}
					}).start();
					
				}
			}
		});
		mnStory.add(mntmBack);
		
		JMenuItem mntmHistory = new JMenuItem("History...");
		mntmHistory.setEnabled(false);
		mnStory.add(mntmHistory);
		
		JMenu mnDebug = new JMenu("Debug");
		menuBar.add(mnDebug);
		
		JMenuItem mntmSelectScene = new JMenuItem("Select Scene...");
		
		mnDebug.add(mntmSelectScene);
		mntmSelectScene.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Scene> sl = new List<Scene>();
				sl.setData(scenes.toArray(new Scene[]{}));
				sl.getComponent().setPreferredSize(new Dimension(400,300));
				
				if(JOptionPane.showConfirmDialog(TwingMain.this, sl.getComponent(), "Select Scene", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE)
						==JOptionPane.OK_OPTION){
					final String s = sl.getJList().getSelectedValue().getName();
					new Thread(new Runnable() {
						public void run() {
							openScene(s, true);
						}
					}).start();
				}
			}
		});
		
		JMenuItem mntmReparseScene = new JMenuItem("Reparse Scene");
		mntmReparseScene.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Thread(new Runnable() {
					public void run() {
						openScene(scene.getName(), false);
					}
				}).start();
			}
		});
		mnDebug.add(mntmReparseScene);
		
		JSeparator separator = new JSeparator();
		mnDebug.add(separator);
		
		JMenuItem mntmAddVariable = new JMenuItem("Add Variable...");
		mntmAddVariable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = JOptionPane.showInputDialog(TwingMain.this, "Enter the name of the new variable.", "New Var", JOptionPane.PLAIN_MESSAGE);
				if(name!=null){
					setVar(name, "0");
				}
			}
		});
		mnDebug.add(mntmAddVariable);
		
		JCheckBoxMenuItem showVarList = new JCheckBoxMenuItem("Show Variable List...");
		mnDebug.add(showVarList);
		
		JMenuItem mntmSource = new JMenuItem("View Source...");
		
		mnDebug.add(mntmSource);
		mntmSource.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						sourceViewer.show(scene.getText(), scene.getName(), TwingMain.this);
					}
				});
				
			}
		});
		
		JMenu mnOptions = new JMenu("Options");
		menuBar.add(mnOptions);
		
		JMenu laf = new JMenu("Look And Feel");
		mnOptions.add(laf);
		
		JMenu mnAbout = new JMenu("About");
		menuBar.add(mnAbout);
		
		JMenuItem licenses = new JMenuItem("Licenses");
		mnAbout.add(licenses);
		
		sourceViewer = new SourceViewer();
		varList = new VarGui();
		
		contentPane = new JPanel();
		contentPane.setBorder(null);
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		///TODO
		//Toolkit.getToolkit().init();
		final JFXPanel p = new JFXPanel();
		p.addComponentListener(new ComponentAdapter(){
			public void componentResized(ComponentEvent e) {
				javafx.application.Platform.runLater(new Runnable() {
					public void run() {
						content.setPrefSize(e.getComponent().getWidth(), e.getComponent().getHeight());
					}
				});
			}
			
		});
		contentPane.add(p, BorderLayout.CENTER);
		javafx.application.Platform.runLater(new Runnable() {
			public void run() {
				//System.out.println("JFX STARTED");
				content = new WebView();
				content.setMinSize(0, 0);
				content.setPrefSize(p.getWidth(), p.getHeight());
				p.setScene(new javafx.scene.Scene(new Group(content)));
				content.getEngine().setOnAlert(new EventHandler<WebEvent<String>>() {
					public void handle(WebEvent<String> arg0) {
						JOptionPane.showMessageDialog(TwingMain.this, arg0.getData(), "Javascript Alert", JOptionPane.INFORMATION_MESSAGE);
					}
				});
				content.getEngine().setOnError(new EventHandler<WebErrorEvent>() {
					public void handle(WebErrorEvent arg0) {
						JOptionPane.showMessageDialog(TwingMain.this, arg0.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
						
					}
				});
				//content.getEngine().
				System.out.println("INIT OK");
				//content.getEngine().executeScript("alert(\"JS OK\");");
				return;
			}
			
		});
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		sceneName = new JTextField();
		panel.add(sceneName);
		sceneName.setEditable(false);
		sceneName.setColumns(10);
		new Thread(new Runnable() {
			public void run() {
				try {
					File story = new File(config.get("game", "C:/Users/Patrick/Downloads/Unplanned 1.0.html"));
					if(args.length >0){
						if(new File(args[0]).exists()){
							story = new File(args[0]);
						}
					}
					openStory(story, true);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();
		setVisible(true);
		showVarList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean b = ((JCheckBoxMenuItem)e.getSource()).isSelected();
				varList.toggleVisible();
			}
		});
		licenses.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new LicenseGui(TwingMain.this);
			}
		});
		ActionListener lafProc = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				config.setGroup("ui");
				config.set("lookandfeel", e.getActionCommand());
				config.setGroup("General");
				JOptionPane.showMessageDialog(TwingMain.this, "Please restart the application to apply the new look and feel.", "Restart Required", JOptionPane.INFORMATION_MESSAGE);
			}
		};
		LookAndFeelInfo[] lafs = UIManager.getInstalledLookAndFeels();
		ButtonGroup lafsB = new ButtonGroup();
		for(LookAndFeelInfo lafi : lafs){
			JRadioButtonMenuItem lafI = new JRadioButtonMenuItem(lafi.getName());
			lafI.setActionCommand(lafi.getClassName());
			lafI.setSelected(UIManager.getLookAndFeel().getClass().getName().equals(lafi.getClassName()));
			lafsB.add(lafI);
			lafI.addActionListener(lafProc);
			laf.add(lafI);
		}
	}
	
	public void openStory(File file, boolean doInit) throws IOException{
		setTitle("SJTPE - "+file.getName()+" - Twine Viewer");
		
		game = file;
		scenes.clear();
		vars.clear();
		widgets.clear();
		while(content==null){
			sceneName.setText(storyName +  " .. Waiting for jfx: " + content);
		}
		javafx.application.Platform.runLater(new Runnable() {
			public void run() {
				content.getEngine().loadContent("<html><body><div id=\"sscene\">Loading Story...</div></body></html>");
			}
		});
		runScript("var macros={};var state={\"history\":[{\"variables\":{}}]}; var SJTPETextBuffer = \"\"; function Wikifier(place, text){SJTPETextBuffer+=text;}"
				+ "\nvar postrender = {};var prerender = {};function returnVars(){var varArray= [];"
				+ "var vars =state.history[0].variables;for (var key in vars) "
				+ "{if (vars.hasOwnProperty(key)) {varArray.push(key);varArray.push(vars[key]);}}return varArray;}"
				+ "Wikifier.setValue = function(name, value){state.history[0].variables[name] = value;}");
		
		Document doc = Jsoup.parse(file, "UTF-8");
		Element sa = doc.select("#store-area").first();
		if(sa==null){
			sa = doc.select("#storeArea").first();
		}
		if(sa==null){
			JOptionPane.showMessageDialog(TwingMain.this, "Story does not have store area, are you sure it's a twine story? " + doc.select("#store-area").size(), "Read Story Error", JOptionPane.ERROR_MESSAGE);
			
			return;
		}
		Elements es = sa.children();
		
		for(int i = 0 ; i < es.size(); i++){
			Element e = es.get(i);
			String tags = e.attr("tags").toLowerCase();
			if(tags.contains("widget")){
				new Scene("Widget", e.text()).getHTML(true);
			}else if(tags.contains("script")){
				String script = e.text().replace("\\n", "\n").replace("\\t", "");
				System.out.println(script);
				runScript(script);
			}else{
				scenes.add(new Scene(e.attr("tiddler"), e.text()));
			}
			
		}
		Scene name = getScene("StoryTitle");
		if(name!=null){
			storyName = name.getText();
		}else{
			storyName = "Unnamed";
		}
		//runScript("alert(macros);");
		if(doInit){
			openScene("Start", true);
		}
		
		config.set("game", file.getAbsolutePath());
	}
	
	public static void openScene(String name, boolean history){
		javafx.application.Platform.runLater(new Runnable() {
			public void run() {
				sceneName.setText(storyName + ": "+name + " .. parsing");
		String[] splt = name.split("%EXEC:");
		if(splt.length > 1){
			Scene exec = new Scene("name", splt[1]);
			exec.getHTML(true);
		}
		//for(int i = 0; i < splt.length; i++){
		//	if(splt[i].startsWith("\""))splt[i] = splt[i].substring(1);
		//	if(splt[i].endsWith("\""))splt[i] = splt[i].substring(0, splt[i].length()-1);
		//}
		Scene s = getScene(splt[0]);
		if(s!=null){
			scene = s;
			JSObject vars = (JSObject)runScript("function doPreRender(){for(var m in prerender){if(typeof prerender[m]==\"function\"){prerender[m]();}}return returnVars();}doPreRender();");
			//System.out.println(vars.getMember("length"));
			int jsVarLength = (int)vars.getMember("length");
			for(int i = 0; i < jsVarLength; i+=2){
				String name = (String)vars.getSlot(i);
				String value = vars.getSlot(i+1).toString();
				if(TwingMain.vars.get(name)==null || !TwingMain.vars.get(name).equals(value))setVar(name, value, false);
				
			}
			sceneName.setText(storyName + ": "+s.getName() + " .. parsing scene");
			String html = s.getHTML(false);
			while(content==null){
				sceneName.setText(storyName + ": "+s.getName() + " .. Waiting for jfx: " + content);
			}
			sceneName.setText(storyName + ": "+s.getName() + " .. loading");
			//javafx.application.Platform.runLater(new Runnable() {
				//public void run() {
					System.out.println(html);
					
					//content.getEngine().loadContent(html);
					runScript("document.getElementById('sscene').innerHTML=\""+html.replace("\"", "\\\"")+"\"");
					
					EventListener openSceneEvent = new EventListener() {
                        public void handleEvent(org.w3c.dom.events.Event ev) {
                            String domEventType = ev.getType();
                            if (domEventType.equals("click")) {
                                String href = ((org.w3c.dom.Element)ev.getTarget()).getAttribute("href");
                                if(href.startsWith("SCENE:")){
            						new Thread(new Runnable() {
            							public void run() {
            								openScene(href.substring(6), true);
            							}
            						}).start();
            					}
                            } 
                        }
                    };
                    EventListener setVariableEvent = new EventListener() {
                        public void handleEvent(org.w3c.dom.events.Event ev) {
                            String domEventType = ev.getType();
                            if (domEventType.equals("click")) {
                                String value = ((org.w3c.dom.Element)ev.getTarget()).getAttribute("value");
                                String name = ((org.w3c.dom.Element)ev.getTarget()).getAttribute("name");
                                if(name==null)name="null";
                                if(name.startsWith("$"))name = name.substring(1);
                                setVar(name, value);
                            } 
                        }
                    };
                    org.w3c.dom.Document doc = content.getEngine().getDocument();
                    NodeList links = doc.getElementsByTagName("a");
                    for (int i = 0; i < links.getLength(); i++) {
                        ((EventTarget) links.item(i)).addEventListener("click", openSceneEvent, false);
                    }
                    NodeList radiobuttons = doc.getElementsByTagName("input");
                    for (int i = 0; i < radiobuttons.getLength(); i++) {
                        ((EventTarget) radiobuttons.item(i)).addEventListener("click", setVariableEvent, false);
                    }
                    
					vars = (JSObject)runScript("function doPostRender(){for(var m in postrender){if(typeof postrender[m]==\"function\"){postrender[m]();}}return returnVars();}doPostRender();");
					//System.out.println(vars.getMember("length"));
					jsVarLength = (int)vars.getMember("length");
					for(int i = 0; i < jsVarLength; i+=2){
						String name = (String)vars.getSlot(i);
						String value = vars.getSlot(i+1).toString();
						if(TwingMain.vars.get(name)==null || !TwingMain.vars.get(name).equals(value))setVar(name, value, false);
						
					}
					sceneName.setText(storyName + ": "+s.getName());
			
			sourceViewer.set(s.getName(), s.getText());
			if(history)TwingMain.history.add(splt[0]);
		}else{
			System.err.println("Scene " + splt[0] + " not found");
		}}});
	}
	
	public static Scene getScene(String name){
		for(int i = 0; i < scenes.size(); i++){
			if(scenes.get(i).getName().equals(name)){
				return scenes.get(i);
			}
		}
		return null;
	}
	
	/**perameter updateJS set as true*/
	public static void setVar(String name, String value){
		setVar(name, value, true);
	}
	
	public static void setVar(String name, String value, boolean updateJS){
		if(doVarSetting){
			vars.put(name, value);
			varList.setVar(name, value);
			if(updateJS)runScript("state.history[0].variables['"+name+"'] = " + (isNum(value)? (value) : ("\"" + value + "\"")) + ";");
			//System.out.println("SetVar:" + name + "=" + value);
		}
	}
	
	public static String runCode(String cmd, TextHandler text){
		String[] splt = cmd.split(" (?=([^\"]*\"[^\"]*\")*[^\"]*$)");
		for(int i = 0; i < cmds.size(); i++){
			if(splt[0].toLowerCase().startsWith(cmds.get(i).getName())){
				return cmds.get(i).run(splt, cmd, text);
			}
		}
		if(cmd.startsWith("$") && vars.containsKey(cmd.substring(1))){
			return vars.get(cmd.substring(1));
		}
		
		//System.out.println(cmd);
		if(widgets.containsKey(cmd)){
			return widgets.get(cmd).getHTML(false);
		}
		if(vars.containsKey(cmd)){
			return vars.get(cmd);
		}
		try{
		Boolean js = (Boolean)runScript("function lookup(){var run = false;var mac = macros['"+cmd+"']; if(mac!=null){"
				+ "mac.handler(0, \""+cmd+"\", [], \"jsparser\");return true;}return false;} lookup();");
		if(js){
			return (String)runScript("function SJTPETextBufferLookup(){var SJTPEBufTemp = SJTPETextBuffer;SJTPETextBuffer=\"\";return SJTPEBufTemp;} SJTPETextBufferLookup();");
		}
		}catch(Exception e){e.printStackTrace();return "<p style=\"color: red;\">Error running command \""+cmd+"\": \"" + e.toString() + "\".</p>";}
		Scene s = getScene(cmd);
		if(s!=null)return s.getHTML(false);
		System.out.println("Command Not Found: \"" + cmd + "\".");
		return "<p style=\"color: red;\">Command Not Found: \"" + cmd + "\".</p>";
	}
	
	public static void saveGame(File path)throws Exception{
		DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(path)));
		out.writeUTF("TWINE_GAME_SAVE_FILE");
		out.writeDouble(1.0);
		out.writeUTF(game.getAbsolutePath());
		out.writeUTF(storyName);
		out.writeUTF(scene.getName());
		out.writeInt(vars.size());
		for(Map.Entry<String, String> e : vars.entrySet()){
			out.writeUTF(e.getKey());
			out.writeUTF(e.getValue());
		}
		out.close();
		config.set(game.getAbsolutePath()+"_lastsave", path.getAbsolutePath());
	}
	
	public static void loadGame(File path)throws Exception{
		DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(path)));
		in.readUTF();
		in.readDouble();
		in.readUTF();
		in.readUTF();
		String scene = in.readUTF();
		int varCount = in.readInt();
		vars.clear();
		for(int i = 0; i < varCount; i++){
			setVar(in.readUTF(), in.readUTF());
		}
		doVarSetting = false;
		openScene(scene, true);
		doVarSetting = true;
		in.close();
	}
	
	public static abstract class Compare{
		public String main;
		public String alt;
		public Compare(String main, String alt){
			this.main = main;
			this.alt = alt;
		}
		public abstract boolean c(String n1, String n2);
	}
	
	public static double num(String value){
		try{
			return Double.parseDouble(value.trim());
		}catch(Exception e){
			try{
				return Integer.parseInt(value.trim());
			}catch(Exception e1){
				return 0;
			}
		}
	}
	
	public static boolean isNum(String value){
		try{
			Double.parseDouble(value.trim());
			return true;
		}catch(Exception e){
			try{
				Integer.parseInt(value.trim());
				return true;
			}catch(Exception e1){
				return false;
			}
		}
	}
	
	public static String parseValue(String s){
		ArrayList<String> values = new ArrayList<String>();
		StringBuilder b = new StringBuilder();
		for(int i = 0; i < s.length(); i++){
			char c = s.charAt(i);
			if(c=='+'||c=='-'||c=='*'||c=='/'){
				values.add(b.toString().trim());
				values.add(c+"");
				b.delete(0, b.length());
			}else{
				b.append(c);
			}
		}
		values.add(b.toString().trim());
		for(int i = 0; i < values.size(); i++){
			String value = values.get(i);
			if(value.startsWith("\"")){
				value = value.substring(1, value.length()-1);
			}else if(value.toLowerCase().endsWith(")")){
				for(int j = 0; j < cmds.size(); j++){
					if(value.toLowerCase().startsWith(cmds.get(j).getName())){
						value = cmds.get(j).run(value.substring(cmds.get(j).getName().length()+1, value.length()-1).split(","), value, null);
					}
				}
			}else if(value.startsWith("$")){
				value = vars.get(value.substring(1));
				if(value==null){System.out.println("Var Not Found("+values.get(i)+")");
					value="";
				}
			}
			if(value!=values.get(i))values.set(i, value);
		}
		for(int i = 1; i < values.size()-1; i++){
			
			String c = values.get(i);//s was already used sue me
			
			//System.out.println("Parsing " + i + " val \"" + c+ "\" " ) ;
			if(c.equals("+")){
				String l=values.get(i-1);values.remove(i-1);i--;
				String f=values.get(i+1);values.remove(i+1);
				//System.out.println("Adding " + i + " val \"" + f +"\", \"" + l + "\"") ;
				if(isNum(f) && isNum(l)){
					//System.out.println("Setting " + i + " to " + (num(f)+num(l))) ;
					values.set(i, num(f)+num(l)+"");
				}
			}
			if(c.equals("-")){
				String l=values.get(i-1);values.remove(i-1);i--;
				String f=values.get(i+1);values.remove(i+1);
				//System.out.println("Adding " + i + " val \"" + f +"\", \"" + l + "\"") ;
				if(isNum(f) && isNum(l)){
					//System.out.println("Setting " + i + " to " + (num(f)+num(l))) ;
					values.set(i, num(l)-num(f)+"");
				}
			}
		}
		//System.out.println("parseValue(" + s + "): "+values.get(0) + " " + ((values.size() > 1)? "[Too Long]" : "Ok"));
		
		return values.get(0);
	}
	
	public static double getRandom(String cmd){
		String[] cmdA = cmd.replace("random", "").replace("(", "").replace(")", "").split(",");
		double r1 = 0;
		double r2 = 0;
		if(cmdA.length > 1){
			r1 = num(parseValue(cmdA[0]));
			r2 = num(parseValue(cmdA[1]));
		}else{
			r2 = num(parseValue(cmdA[0]));
			System.out.println("Warn: rand length invalid for: " + cmd);
		}
		if(r1 > r2){
			double tmp = r2;
			r2 = r1;
			r1 = tmp;
		}
		System.out.println("Rand: " + r2 + ", " + r1 + ", " + (int)(r2-r1+1) + ", " + parseValue(cmdA[0]) +  ", " +parseValue(cmdA[1]));
		int val=random.nextInt((int)(r2-r1+1));
		
		
		return val + (int)r1;
	}
	
	private static boolean scriptRunning;
	private static Object sReturn;
	private static Exception sExcep;
	public static Object runScript(String script){
		scriptRunning = true;
		if(javafx.application.Platform.isFxApplicationThread()){
			return content.getEngine().executeScript(script);
		}
		//while(content==null){
		//	
		//}
		javafx.application.Platform.runLater(new Runnable() {
			public void run() {
				try{
					sReturn = content.getEngine().executeScript(script);
					scriptRunning = false;
				}catch(Exception e){e.printStackTrace();sExcep =e;scriptRunning = false;}
				
			}
		});
		while(scriptRunning){
			try {
				Thread.sleep(64);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		if(sExcep!=null){
			Exception temp = sExcep;sExcep=null;
			throw new RuntimeException(temp);
		}
		return sReturn;
	}

}
