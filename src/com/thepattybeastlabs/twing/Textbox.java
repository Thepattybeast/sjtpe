package com.thepattybeastlabs.twing;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

public class Textbox extends JTextField implements FocusListener, KeyListener{

	private static final long serialVersionUID = -5100936415840254848L;

	private String hint;
	private boolean showingHint;
	private Color fore = Color.BLACK;
	private Color desc = Color.GRAY;
	private Color back = Color.WHITE;
	private ArrayList<TextChangedListener> tcls = new ArrayList<TextChangedListener>();
	private String lastText = "";
	
	public Textbox(String hint){
		this.hint = hint;
		showingHint = true;
		addFocusListener(this);
		focusLost(null);
		addKeyListener(this);
	}
	
	@Override
	public void focusGained(FocusEvent e) {
		if(showingHint){
			super.setText("");
			setFont(getFont().deriveFont(Font.PLAIN));
			setForeground(fore);
			setCaretColor(fore);
			showingHint = false;
		}
			
	}

	@Override
	public void focusLost(FocusEvent e) {
		if(getText().equals("")){
			super.setText(hint);
			showingHint = true;
			setForeground(desc);
			setCaretColor(desc);
			setFont(getFont().deriveFont(Font.ITALIC));
		}
	}
	
	public void setText(String text){
		if(text!=null && !text.isEmpty())focusGained(null);
		else focusLost(null);
		super.setText(text);
		activateTCL(true);
	}
	
	public String getText(){
		if(showingHint)return "";
		return super.getText();
	}
	
	public void setColors(Color fore, Color back, Color desc){
		this.fore = fore;
		this.back = back;
		this.desc = desc;
		setBackground(back);
	}
	
	private void activateTCL(boolean set){
		for(int i = 0; i < tcls.size(); i++){
			tcls.get(i).textChanged(this, getText(), set);
		}
		lastText = getText();
	}
	
	public void addTextChangeListener(TextChangedListener tcl){
		tcls.add(tcl);
	}

	public void keyPressed(KeyEvent arg0) {}

	public void keyReleased(KeyEvent arg0) {
		if(lastText != getText())activateTCL(false);
	}

	public void keyTyped(KeyEvent arg0) {}
	
}
