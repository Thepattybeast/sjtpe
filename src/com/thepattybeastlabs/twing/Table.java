package com.thepattybeastlabs.twing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class Table extends DefaultTableModel{
	
	private Object[][] data = new Object[1][0];
	private String[] headers;
	private Class<?>[] columnClasses;
	private JScrollPane scroll;
	private JTable table;
	private TableRowSorter<TableModel> sorter;
	private boolean[] editable;
	private ArrayList<TableModelListener> ls = new ArrayList<TableModelListener>();
	
	public Table(String[] headers, Class<?>[] columnClasses){
		this(headers, columnClasses, new JTable());
	}
	
	public Table(String[] headers, Class<?>[] columnClasses, JTable table){
		this.headers = headers;
		this.columnClasses = columnClasses;
		this.table = table;
		table.setModel(this);
		sorter = new TableRowSorter<>();
		sorter.setModel(this);
		table.setRowSorter(sorter);
		scroll = new JScrollPane(table);
		setArraySize(3, 0);
	}
	
	public void setEditable(boolean... editable){
		this.editable = editable;
	}
	
	public TableRowSorter<TableModel> getSorter(){
		return sorter;
	}
	
	public JScrollPane getComponent(){
		return scroll;
	}
	
	public JTable getTable(){
		return table;
	}
	
	public void setArraySize(int width, int height){
		table.setSize(width, height);
		data = new Object[width][height];
	}
	
	public void resizeArray(int width, int height){
		Object[][] temp = data;
		data = new Object[width][height];
		table.setSize(width, height);
		for(int x = 0; x < temp.length; x++){
			for(int y = 0; y < temp[0].length; y++){
				data[x][y] = temp[x][y];
			}
		}
	}

	@Override
	public int getRowCount() {
		if(data==null){
			System.out.println("WTF DATA IS NULL");
			return 0;
		}
		if(data.length < 1)return 0;
		return data[0].length;
	}

	@Override
	public int getColumnCount() {
		return headers.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return headers[columnIndex];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return columnClasses[columnIndex];
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return editable[columnIndex];
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		//System.out.println(columnIndex + ", " + rowIndex + ": " + data[columnIndex][rowIndex]);
		if(columnIndex>data.length||rowIndex>data[0].length)return null;
		Object res = null;
		try{res = data[columnIndex][rowIndex];}catch(Exception e){}
		return res;
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		//System.out.println(data.length + " " + columnIndex);
		if(columnIndex >= data.length){System.out.println("Column out of range");return;}
		if(data.length < 1){System.out.println("No Columns");return;}
		if(rowIndex >= data[columnIndex].length){System.out.println("Row out of range");return;}
		data[columnIndex][rowIndex] = aValue;
		for(TableModelListener l : ls){
			//System.out.println(rowIndex + ", " + columnIndex);
			try{
				l.tableChanged(new TableModelEvent(this, rowIndex-1, rowIndex-1, columnIndex, TableModelEvent.UPDATE));
			}catch(Exception e){
				e.printStackTrace();
			}	
		}
	}

	public void addTableModelListener(TableModelListener l) {
		ls.add(l);
		System.out.println("ADDED TBL MDL LISTNER");
	}

	public void removeTableModelListener(TableModelListener l) {
		ls.remove(l);
	}
	
}
