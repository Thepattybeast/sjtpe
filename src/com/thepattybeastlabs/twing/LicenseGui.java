package com.thepattybeastlabs.twing;

import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

public class LicenseGui {

	private JFrame frame;
	private JTabbedPane tabs;
	
	public LicenseGui(JFrame parent){
		frame = new JFrame("Licenses");
		frame.setSize(800, 600);
		frame.setLocationRelativeTo(parent);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		tabs = new JTabbedPane();
		add("JSoup License", JSOUP.replace("/n", System.getProperty("line.separator")));
		frame.add(tabs);
		frame.setVisible(true);
	}
	
	private void add(String name, String content){
		JTextArea text = new JTextArea(content);
		text.setEditable(false);
		text.setLineWrap(true);
		text.setWrapStyleWord(true);
		text.setFont(new Font("Tahoma", 0, 14));
		tabs.add(name, new JScrollPane(text));
	}
	
	private static final String JSOUP = "jsoup License/nThe jsoup code-base (include source and compiled packages) are distributed under "
			+ "the open source MIT license as described below./n/nThe MIT License/n"
			+ "Copyright � 2009 - 2013 Jonathan Hedley (jonathan@hedley.net)/n/nPermission is hereby granted, "
			+ "free of charge, to any person obtaining a copy of this software and associated documentation files (the \"Software\"), "
			+ "to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, "
			+ "sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following "
			+ "conditions:/n/nThe above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software."
			+ "/n/nTHE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES "
			+ "OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE "
			+ "FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION "
			+ "WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE./n/n";
	
}
