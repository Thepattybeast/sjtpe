package com.thepattybeastlabs.twing;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class SourceViewer {

	private JFrame frame;
	private JTextPane text;
	private JComboBox<String> scenes;
	
	public SourceViewer(){
		scenes = new JComboBox<>();
		scenes.setEditable(true);
		for(int i = 0; i < TwingMain.scenes.size(); i++){
			scenes.addItem(TwingMain.scenes.get(i).getName());
		}
		frame = new JFrame("View Source");
		frame.setSize(600, 400);
		text = new JTextPane();
		frame.setLocationRelativeTo(null);
		frame.add(new JScrollPane(text));
		frame.add(scenes, BorderLayout.NORTH);
		scenes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				set("Update", TwingMain.getScene(scenes.getSelectedItem().toString()).getText());
			}
		});
	}
	
	public void show(String text, String title, JFrame parent){
		this.text.setText(text.replace(new String(new char[]{'\\','n'}), "\n"));
		frame.setTitle(title + " - Source");
		frame.setLocationRelativeTo(parent);
		frame.setVisible(true);
		scenes.removeAllItems();
		for(int i = 0; i < TwingMain.scenes.size(); i++){
			scenes.addItem(TwingMain.scenes.get(i).getName());
		}
		scenes.setSelectedItem(title);
	}

	public void set(String name, String text) {
		this.text.setText(text.replace(new String(new char[]{'\\','n'}), "\n"));
		scenes.setSelectedItem(name);
		frame.setTitle(name + " - Source");
	}
	
}
