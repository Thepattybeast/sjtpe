package com.thepattybeastlabs.twing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JRadioButton;

public class RadioButtonGroup implements ActionListener{

	private String name;
	private ArrayList<JRadioButton> buttons;
	private int sel = 0;
	
	public RadioButtonGroup(String name){
		this.name = name;
		buttons = new ArrayList<>();
	}
	
	public String getName(){
		return name;
	}
	
	public void add(JRadioButton button){
		if(buttons.size()==0)button.setSelected(true);
		buttons.add(button);
		button.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if(buttons.size() == 1)setSelected(buttons.get(0).isSelected()? 0 : -1);
		else setSelected(buttons.indexOf(e.getSource()));
	}
	
	public void setSelected(int button){
		sel = button;
		for(int i = 0; i < buttons.size(); i++){
			buttons.get(i).setSelected(i==button);
		}
		TwingMain.setVar(name.replace("\"", "").replace("$", ""), sel > -1?(TwingMain.parseValue(buttons.get(sel).getName())): "");
	}
	
}
