package com.thepattybeastlabs.twing;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Scanner;

public class INIFile {
	
	private String path;
	private ArrayList<Group> groups;
	private Group current;
	private boolean saveOnSet;
	
	public INIFile(String path) throws FileNotFoundException{
		this.path = path;
		groups = new ArrayList<Group>();
		groups.add(new Group("General"));
		current = groups.get(0);
		if(new File(path).exists())load();
		
	}
	
	/**
	 * @return The Active Group*/
	public Group getGroup(){
		return current;
	}
	
	public void deleteGroup(String name){
		groups.remove(getGroup(name));
	}
	
	public boolean hasGroup(String name){
		for(int i = 0; i < groups.size(); i++){
			if(groups.get(i).name.equalsIgnoreCase(name))return true;
		}
		return false;
	}
	
	/**Note: if the group does not exist it will be created
	 * @return The Group With The Specified Name
	 * 
	 * @param name The name of the group to return*/
	public Group getGroup(String name){
		for(int i = 0; i < groups.size(); i++){
			if(groups.get(i).name.equalsIgnoreCase(name))return groups.get(i);
		}
		Group group = new Group(name);
		groups.add(group);
		return group;
	}
	
	public Group[] getGroups(){
		return groups.toArray(new Group[]{});
	}
	
	public boolean setGroup(String name){
		if(getGroup(name)!=null){
			current = getGroup(name);
			return true;
		}
		//System.out.println("Created Group(name=\""+name+"\")");
		Group group = new Group(name);
		groups.add(group);
		current = group;
		return false;
	}
	
	public void print(){
		System.out.println("------------------[INI FILE AT: "+ path +"]------------------");
		System.out.println(toString());
		System.out.println("------------------[END FILE AT: "+ path +"]------------------");
	}
	
	public String toString(){
		String res = "";
		for(int i = 0; i< groups.size(); i++){
			res+=groups.get(i);
			if(i!=groups.size()-1)res+="\n\n";
		}
		return res;
	}
	
	//set operators
	
	public String get(String name, String defaultValue){
		return getGroup().get(name, defaultValue);
	}
	
	public int get(String name, int defaultValue){
		try{
			return Integer.parseInt(get(name, defaultValue+""));
		}catch(Exception e){}
		return defaultValue;
	}
	
	public long get(String name, long defaultValue){
		try{
			return Long.parseLong(get(name, Long.toString(defaultValue)));
		}catch(Exception e){}
		return defaultValue;
	}
	
	public boolean get(String name, boolean defaultValue){
		try{
			return Boolean.parseBoolean(get(name, defaultValue+""));
		}catch(Exception e){}
		return defaultValue;
	}
	
	public void set(String name, String value){
		getGroup().setTag(name, value);
		if(saveOnSet){
			try {
				save();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	//File Operators
	
	/**Load From The File
	 * Called In Constructor(If File Exists)*/
	public void load() throws FileNotFoundException{
		//System.out.println("Loading INI File At " + path + "...");
		Scanner s = new Scanner(new File(path));
		setGroup("General");
		while(s.hasNext()){
			String line = s.nextLine();
			if(line.startsWith("[") && line.endsWith("]")){
				setGroup(line.replace("[", "").replace("]", ""));
			}else if(line.contains("=")){
				String name = "";
				String value = "";
				String[] data = line.split("=");
				name = data[0];
				for(int i = 1; i < data.length; i++){
					value+=data[i];
					if(i<data.length-1)value+="=";
				}
				set(name, value.trim());
			}
			
		}
		s.close();
		//System.out.println("Done");
	}
	
	/**Saves To The File*/
	public void save() throws FileNotFoundException{
		//System.out.println("Saving..." + path);
		String[] file = toString().split("\n");
		Formatter f = new Formatter(new File(path));
		for(int i = 0; i < file.length; i++){
			f.format("%s\n", file[i]);
		}
		f.close();
		//System.out.println("Done");
	}
	
	public void setSaveOnSet(boolean save) {
		saveOnSet = save;
	}
	
	/**Contains the information of an individual field*/
	public class Tag{
		private String name;
		private String info;
		
		public Tag(String name, String info) {
			
			this.name = name;
			setInfo(info);
		}
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getInfo() {
			return info;
		}
		public void setInfo(String info) {
			if(info == null)info = "null";
			this.info = info;
		}
		public String toString(){
			return name+"="+info;
		}
		
	}
	
	public class Group{
		
		private ArrayList<Tag> tags;
		private String name;
		
		public Group(String name){
			tags = new ArrayList<Tag>();
			this.name = name;
		}
		
		public void setTag(String name, String value){
			if(getTag(name)==null)tags.add(new Tag(name, value));
			else getTag(name).setInfo(value);
		}
		
		public Tag getTag(String name){
			for(int i = 0; i < tags.size(); i++){
				if(tags.get(i).getName().equalsIgnoreCase(name)){
					return tags.get(i);
				}
			}
			return null;
		}
		
		public void setName(String name){
			this.name = name;
		}
		
		public String getName(){
			return name;
		}
		
		public String get(String tagName, String defaultValue){
			if(getTag(tagName)!=null)return getTag(tagName).getInfo();
			return defaultValue;
		}
		
		public List<Tag> getTags(){
			return tags;
		}
		
		public String toString(){
			String res="";
			res+="["+name+"]\n";
			for(int i = 0; i < tags.size(); i++){
				res+=tags.get(i);
				if(i!=tags.size()-1)res+="\n";
			}
			return res;
		}
	}

}
