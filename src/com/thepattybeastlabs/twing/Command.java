package com.thepattybeastlabs.twing;

import com.thepattybeastlabs.twing.Scene.TextHandler;

public interface Command {

	public String getName();
	public String run(String[] args, String cmd, TextHandler t);
	
}
