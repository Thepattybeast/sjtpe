package com.thepattybeastlabs.twing;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JRadioButton;

import sun.misc.BASE64Decoder;

public class Scene {

	private String name;
	private String text;
	public Scene parent;
	private boolean comment;
	public Scene(String name, String text) {
		this.name = name;
		this.text = text;
		//JOptionPane.showMessageDialog(null, new JTextArea(text.replace("\\n", "\n")), name, JOptionPane.QUESTION_MESSAGE);
	}
	public String getName() {
		return name;
	}
	public String getText() {
		return text;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	public String toString(){
		return name;
	}
	public String getHTML(boolean tag) {
		comment = false;
		StringBuilder b = new StringBuilder(text.length());
		if(tag)b.append("<html>");
		//b.append("<form action= \"\" >");
		for(int i = 0; i < text.length(); i++){
			char  c = text.charAt(i);
			if(c=='\\' && t(i+1)=='n'){
				i+=1;b.append("<br>");
				
			}else if(c=='\\' && t(i+1)=='s'){
					i+=3;
			}else if(comment && c=='%' && t(i+1)=='/'){
				comment = false;i++;
			}else if(c=='/' && t(i+1)=='%'){
				comment = true;
			}else if(comment){
			}else if(c=='<' && t(i+1)=='<'){
			
				CodeRes r = parseCode(i+2, text);
				if(r!=null){
					b.append(r.text);
					//System.out.println("APPENDING: "+ (r.length+1) + ", " + text.substring(i+2));
					i+=r.length+1;
					//JOptionPane.showMessageDialog(null, new JTextArea("Parse Area: " + t(i-1) + t(i) + t(i+1)+"\n"+r.text.replace("<br>", "<br>\n ")), name, JOptionPane.QUESTION_MESSAGE);
				}
			}else if(c=='[' && t(i+1)=='['){
				Link l = parseLink(i+2, text);
				i+=l.length+1;
				b.append("<a href=\"");b.append("SCENE:");b.append(l.story);
				b.append("\">");b.append(l.text);b.append("</a>");
			}else if(c=='[' && t(i+1)=='i' && t(i+2)=='m' && t(i+3)=='g' && t(i+4)=='['){
				i+=4;
				String lnk = text.substring(i+1);
				lnk = lnk.substring(0, lnk.indexOf("]]"));
				i+=lnk.length()+2;
				Scene s = TwingMain.getScene(lnk);
				if(s!=null){
					b.append("<img src=\"");try {
						b.append(parseImage(s.getText(), s.getName()));
					} catch (IOException e) {
						e.printStackTrace();
					}
					b.append("\"></img>");
				}else if(new File(TwingMain.game.getParentFile().getAbsolutePath()+"/"+lnk).exists()){
					b.append("<img src=\"file:///");b.append(new File(TwingMain.game.getParentFile().getAbsolutePath()+"/"+lnk).getAbsolutePath());b.append("\"></img>");
				}else{
					b.append("[Image scene Not Found["+lnk+"] & ["+new File(TwingMain.game.getParentFile().getAbsolutePath()+"/"+lnk).getAbsolutePath()+"] does not exist]");
				}
			}else{
				b.append(c);
				//System.out.println("char: " + c);
			}
		}
		//b.append("</form>");
		if(tag)b.append("</html>");
		//JOptionPane.showMessageDialog(null, new JTextArea("TEXT OUT: \n" + text.replace("\\n", "\n")
		//+"HTML OUT: \n" + b.toString().replace("<br>", "<br>\n")), name, JOptionPane.QUESTION_MESSAGE);
		return b.toString();
	}
	
	private String parseImage(String text2, String name) throws IOException {
		File save = new File("images/"+TwingMain.storyName + "/" + name + "_Twing_Image");
		if(!save.exists()){
			new File("images/"+TwingMain.storyName).mkdirs();
			BASE64Decoder dec = new BASE64Decoder();
			byte[] b = dec.decodeBuffer(text2.substring(text2.indexOf(',')+1));
			FileOutputStream out = new FileOutputStream(save);
			out.write(b);
			out.close();
		}
		//return save.getAbsolutePath();
		return "file:///"+save.getAbsolutePath();
	}
	
	/**retrun char at i in text*/
	public char t(int i){
		return safeChar(i, text);
	}
	
	public static char safeChar(int pos, String s){ 
		if(pos > -1 && pos < s.length()){
			return s.charAt(pos);
		}
		return '\0';
	}
	
	public Link parseLink(int offset, String text){
		StringBuilder name = new StringBuilder();
		StringBuilder story = new StringBuilder();
		boolean half = false;
		for(int i = offset; i < text.length(); i++){
			char c = text.charAt(i);
			if(c=='|'){
				half = true;
			}else if(c==']' && safeChar(i+1, text)==']'){
				if(half ==false)story.append(name);
				
				return new Link(name.toString(), story.toString(), i-offset+2);
			}else{
				if(half){
					story.append(c);
				}else{
					name.append(c);
				}
			}
		}
		return null;
	}
	
	public CodeRes parseCode(int offset, String text){
		StringBuilder cmd = new StringBuilder();
		for(int i = offset; i < text.length(); i++){
			char c = text.charAt(i);
			if(c=='>' && safeChar(i+1, text)=='>'){
				TextHandler t = new TextHandler();
				t.s = this;
				t.offset = i+2;
				t.text = text;
				String res = TwingMain.runCode(cmd.toString(), t);

				return new CodeRes(res, t.toffset + (i-offset) + 2);
			}else{
				cmd.append(c);
			}
		}
		return null;
	}
	
	public class Link{
		public String text;
		public String story;
		public int length;
		public Link(String text, String story, int length) {
			this.text = text;
			this.story = story;
			this.length = length;
		}
		
	}
	
	public class CodeRes{
		public String text;
		public int length;
		public CodeRes(String text, int length) {
			this.text = text;
			this.length = length;
		}
		
	}
	
	public class TextHandler{
		//public int length;
		public int offset;
		public int toffset;
		public String text;
		public Scene s;
		public char t(int i){
			return safeChar(i, text);
		}
		public char n(){
			toffset++;
			//length++;
			return t(offset+toffset-1);
		}
		public char n(int i){
			return t(offset+toffset-1 + i);
		}
		public boolean next(String str, int off){
			boolean res = true;
			for(int i = 0; i < str.length(); i++){
				if(n(i+off)!=str.charAt(i)){
					res = false;
				}
			}
			return res;
		}
		
	}
	
}
