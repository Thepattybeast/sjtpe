package com.thepattybeastlabs.twing;

public interface TextChangedListener {
	
	public void textChanged(Textbox parent, String newText, boolean set);
	
}
