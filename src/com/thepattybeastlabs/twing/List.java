package com.thepattybeastlabs.twing;

import java.awt.Component;
import java.awt.TextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.Array;
import java.util.ArrayList;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;

public class List<T> {
	
	private T[] data;
	private JList<T> list;
	private JScrollPane pane;
	private Textbox search;
	private boolean searchEnabled = true;
	
	public List(){
		pane = new JScrollPane();
		list = new JList<T>();
		pane.setViewportView(list);
		search = new Textbox("Search...");
		pane.setColumnHeaderView(search);
		search.addKeyListener(new KeyListener() {
			public void keyTyped(KeyEvent arg0) {
				
			}
			public void keyReleased(KeyEvent arg0) {search(search.getText());}
			public void keyPressed(KeyEvent arg0) {}
		});
	}
	
	public JScrollPane getComponent(){
		return pane;
	}
	
	
	
	public void setData(T[] data){
		this.data = data;
		list.setListData(data);
		if(!search.getText().isEmpty()){
			search(search.getText());
		}
	}
	
	public T[] getData(){
		return data;
	}
	
	public T getItem(int index){
		return data[index];
	}
	
	private void search(String text){
		if(text.equals("")){
			list.setListData(data);
			return;
		}
		text = text.toLowerCase();
		ArrayList<T> res = new ArrayList<T>();
		for(int i = 0; i < data.length; i++){
			
			if(data[i].toString().toLowerCase().contains(text)){
				res.add(data[i]);
			}
		}
		list.setListData((T[])res.toArray());
	}
	
	public void setSearchboxEnabled(boolean enabled){
		if(enabled){
			if(!searchEnabled){
				pane.setColumnHeaderView(search);
			}
				
		}else{
			if(searchEnabled){
				pane.setColumnHeaderView(null);
			}
			
		}
		searchEnabled = enabled;
	}
	
	public JList<T> getJList(){
		return list;
	}
	
	public Textbox getSearch(){
		return search;
	}
	
	public void setCellRenderer(ListCellRenderer<T> renderer){
		list.setCellRenderer(renderer);
	}
	
}
