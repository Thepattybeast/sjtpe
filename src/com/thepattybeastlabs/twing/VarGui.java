package com.thepattybeastlabs.twing;

import javax.swing.JFrame;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

public class VarGui {

	private JFrame frame;
	private Table table;
	private boolean doUpdate;
	
	public VarGui(){
		frame = new JFrame("Variable List");
		frame.setSize(800, 600);
		frame.setLocationRelativeTo(null);
		table = new Table(new String[]{"Name", "Value", "Numeric Value", "Is Number"}, new Class[]{String.class, String.class, Double.class, Boolean.class});
		table.setEditable(false, true, false, false);
		frame.add(table.getComponent());
		table.addTableModelListener(new TableModelListener() {
			public void tableChanged(TableModelEvent e) {
				if(doUpdate){
					//System.out.println("Table Content Update: " + e.getFirstRow() + ", " +table.getValueAt(e.getFirstRow()+1, 0));
					TwingMain.setVar(table.getValueAt(e.getFirstRow()+1, 0).toString(), table.getValueAt(e.getFirstRow()+1, 1).toString());
				}
			}
		});
	}
	
	public void toggleVisible(){
		frame.setVisible(!frame.isVisible());
	}
	
	public void setVar(String name, String value){
		doUpdate = false;
		for(int i = 0; i < table.getRowCount(); i++){
			if(table.getValueAt(i, 0)!=null&&table.getValueAt(i, 0).toString().equalsIgnoreCase(name)){
				table.setValueAt(value, i, 1);
				table.setValueAt(TwingMain.num(value), i, 2);
				table.setValueAt(TwingMain.isNum(value), i, 3);
				table.getSorter().allRowsChanged();
				table.getTable().repaint();
				doUpdate = true;
				return;
			}
		}
		table.resizeArray(table.getColumnCount(),table.getRowCount()+1);
		table.setValueAt(name, table.getRowCount()-1, 0);
		table.setValueAt(value, table.getRowCount()-1, 1);
		table.setValueAt(TwingMain.num(value), table.getRowCount()-1, 2);
		table.setValueAt(TwingMain.isNum(value), table.getRowCount()-1, 3);
		table.getSorter().allRowsChanged();
		table.getSorter().rowsUpdated(table.getRowCount()-1, table.getRowCount()-1);
		table.getTable().repaint();
		doUpdate = true;
	}
	
}
